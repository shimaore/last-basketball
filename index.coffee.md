    module.exports = (plugin) ->
      plugin.setOptions dev: true

      plugin.registerCommand 'Uptime', ( ->
        buffer = await plugin.nvim.buffer
        lines = await buffer.lines
        buffer.setLines "Hello world!",
          start: 0
          end: -1
          strictIndexing: true
      ), sync: false
